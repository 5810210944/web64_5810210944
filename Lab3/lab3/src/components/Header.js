import './Header.css';

function Header() {

    return (
        <div id='HeaderBG'>
            <h1>นี่คือส่วนหัวของเว็บ</h1>
            <hr />
        </div>
    );
}

export default Header;