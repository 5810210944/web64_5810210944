const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi', (req, res) => {

    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if ( !isNaN(weight) && !isNaN(height) ){
        let bmi = weight / (height * height)

        result = {
            "status"    :   200,
            "bmi"   :   bmi
        }
    }else {
        result = {
            "status"    :   400,
            "message"   :   "Weight or Height is not a number"
        }
    }

    res.send(JSON.stringify(result))
})

app.get('/hello', (req, res) => {
    res.send('Sawasdee '+ req.query.name)
  })

app.get('/triangle', (req, res) => {
    let height = parseFloat(req.query.height)
    let base = parseFloat(req.query.base)
    var result = {}

    if ( !isNaN(height) && !isNaN(base) ){
        let area = 1 / 2 * (base * height)

        result = {
            "Area"  :   area
        }
    }else {
        result = {
            "status"    :   400,
            "message"   :   "Height or Base is not a number"
        }
    }

    res.send(JSON.stringify(result))
})

app.post('/score', (req, res) => {
    let score = parseFloat(req.query.score)
    var result = {}

    if ( !isNaN(score) ){
        if( score < 50 ) {
            result = {
                "Grade"    :   "E"
            }
        }else if ( score < 55 ) {
            result = {
                "Grade"    :   "D"
            }
        }else if ( score < 60 ) {
            result = {
                "Grade"    :   "D+"
            }
        }else if ( score < 70 ) {
            result = {
                "Grade"    :   "C"
            }
        }else if ( score < 75 ) {
            result = {
                "Grade"    :   "C+"
            }
        }else if ( score < 80 ) {
            result = {
                "Grade"    :   "B"
            }
        }else if ( score < 85 ) {
            result = {
                "Grade"    :   "B+"
            }
        }else {
            result = {
                "Grade"    :   "A"
            }
        }
    }else {
        result = {
            "status"    :   400,
            "message"   :   "Score is not a number"
        }
    }

    res.send(req.query.name + JSON.stringify(result))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})