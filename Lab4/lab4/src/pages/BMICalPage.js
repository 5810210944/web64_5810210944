
import BMIResult from "../components/BMIResult";
import { useState } from "react";
import './BMICalPage.css';

function BMICalPage() {
    const [ name, setName ] = useState("");
    const [ bmiResult, setBmiResult ] = useState(0);
    const [ translateResult, setTranslateResult ] = useState("");
    const [ heigh, setHeigh ] = useState("");
    const [ weight, setWeight ] = useState("");

    function CalculateBMI() {
        let h = parseFloat(heigh);
        let w = parseFloat(weight);
        let bmi = ( w / ( h * h / 10000 ));

        setBmiResult( bmi );

        if ( bmi < 18.5 ) {
            setTranslateResult("ต่ำกว่าเกณฑ์");
        }else if ( bmi < 23 ) {
            setTranslateResult("สมส่วน");
        }else if ( bmi < 25 ) {
            setTranslateResult("น้ำหนักเกิน");
        }else if ( bmi < 30 ) {
            setTranslateResult("โรคอ้วน");
        }else {
            setTranslateResult("โรคอ้วนอันตราย");
        }
    }

    return (
        <div align="left">
            <div align="center">
                <h1>ยินดีต้อนรับสู่เว็บคำนวณ BMI</h1>
                <hr />
                <h4>
                คุณชื่อ: <input type="text" value={name} onChange={ (e) => { setName(e.target.value); }} /> <br />
                ส่วนสูง: <input type="text" value={heigh} onChange={ (e) => { setHeigh(e.target.value); }} /> ซม. <br />
                น้ำหนัก: <input type="text" value={weight} onChange={ (e) => { setWeight(e.target.value); }} /> กก. <br />
                </h4>

                <button onClick={ () => { CalculateBMI() } } > Calculate </button>

                {   ( bmiResult != 0 ) && 
                        <div>
                            <hr />
                            <h1>นี่คือผลคำนวณ</h1>
                            <BMIResult 
                                name = { name }
                                bmi = { bmiResult }
                                result = { translateResult }
                            />
                        </div>
                }
            </div>
        </div>
    );
}

export default BMICalPage;