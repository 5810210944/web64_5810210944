import './AboutUsPage.css';
import AboutUs from "../components/AboutUs";
function AboutUsPage() {
    return (
        <div>
            <div align="center">
                <h2>คณะผู้จัดทำ เว็บนี้</h2>
                <AboutUs name="เศรษฐฉัตร" 
                address="ไม่รับการติดต่อ"
                province="ไม่บอก" />
                <hr />

                <AboutUs name="ไม่รู้ใครเหมือนกัน" 
                address="ไม่รับการติดต่อ"
                province="ไม่บอก" />
            </div>
        </div>
    );
}

export default AboutUsPage;