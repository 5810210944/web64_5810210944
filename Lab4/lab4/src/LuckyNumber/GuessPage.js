import { useState } from "react";
import Guess from "../components/Guess";

function GuessPage() {

    const [ number, setNumber ] = useState("");
    const [ luck, setLuck ] = useState("");
    const [ hide, setHide ] = useState("");

    function Calculate() {
        let n = parseInt(number);
        setHide(n);

        if(n == 69) {
            setLuck("ถูกแล้วจ้า");
        }else {
            setLuck("ผิด");
        }

    }

    return(
        <div>
            กรุณาทายตัวเลขที่ต้องการ ระหว่าง 0-99: <input type="text" value={number} onChange={ (e) => { setNumber(e.target.value); }} /> <br /><br />

            <button onClick={ () => { Calculate() } } > ทาย </button>

            {   ( hide != 0 ) && 
                        <div>
                            <hr />
                            <Guess 
                                number = { number }
                                result = { luck }
                            />
                        </div>
            }

        </div>
    );
}

export default GuessPage;