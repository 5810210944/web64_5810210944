import './Header.css';
import { Link } from "react-router-dom";
function Header() {
    return (
        <div align='left' id='HeaderBG'>
            ยินดีต้อนรับสู้เว็บคำนวณ BMI : &nbsp;&nbsp;
                    <Link to="/">เครื่องคิดเลข</Link>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to="/about">ผู้จัดทำ</Link>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to="/guess">ทายเลข</Link>
            <hr />
        </div>
    );
}

export default Header;