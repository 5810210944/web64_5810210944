import './BMIResult.css';
function BMIResult (props) {
    return (
        <div>
            <h3>คุณ: {props.name}</h3>
            <h2>มี BMI: {props.bmi}</h2>
            <h2>แปลว่า: {props.result}</h2>
        </div>
    );
}

export default BMIResult;