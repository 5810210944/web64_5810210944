import './AboutUs.css';
function AboutUs (props) {

    return (
        <div>
            <h5>จัดทำโดย: {props.name}</h5>
            <h6>ติดต่อได้ที่: {props.address}</h6>
            <h6>บ้านอยู่ที่: {props.province}</h6>
        </div>
    );
}

export default AboutUs;