import './Header.css';
import { Link } from "react-router-dom";

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
            <Typography variant="h6"> ยินดีต้อนรับสู้เว็บคำนวณ BMI : </Typography>
            
            &nbsp;&nbsp;

            <Link to="/">
                <Typography variant="body1"> เครื่องคิดเลข </Typography>
            </Link>

            &nbsp;&nbsp;

            <Link to="/about">
                <Typography variant="body1"> ผู้จัดทำ </Typography>
            </Link> 

            &nbsp;&nbsp;

            <Link to="/guess">
                <Typography variant="body1"> ทายเลข </Typography>
            </Link>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header;

/*function Header() {
    return (
        <div align='left' id='HeaderBG'>
            ยินดีต้อนรับสู้เว็บคำนวณ BMI : &nbsp;&nbsp;
                    <Link to="/">เครื่องคิดเลข</Link>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to="/about">ผู้จัดทำ</Link>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to="/guess">ทายเลข</Link>
            <hr />
        </div>
    );
}

export default Header;*/