import './AboutUs.css';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUs (props) {

    return (
        <Box sx={{ width : "60%" }}>
            <Paper elevation={3}>
                <h5>จัดทำโดย: {props.name}</h5>
                <h6>ติดต่อได้ที่: {props.address}</h6>
                <h6>บ้านอยู่ที่: {props.province}</h6>
            </Paper>
        </Box>
    );
}

export default AboutUs;