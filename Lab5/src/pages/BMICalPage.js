
import BMIResult from "../components/BMIResult";
import { useState } from "react";
import './BMICalPage.css';

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from "@mui/material";
import Paper from '@mui/material/Paper';

function BMICalPage() {
    const [ name, setName ] = useState("");
    const [ bmiResult, setBmiResult ] = useState(0);
    const [ translateResult, setTranslateResult ] = useState("");
    const [ heigh, setHeigh ] = useState("");
    const [ weight, setWeight ] = useState("");

    function CalculateBMI() {
        let h = parseFloat(heigh);
        let w = parseFloat(weight);
        let bmi = ( w / ( h * h / 10000 ));

        setBmiResult( bmi );

        if ( bmi < 18.5 ) {
            setTranslateResult("ต่ำกว่าเกณฑ์");
        }else if ( bmi < 23 ) {
            setTranslateResult("สมส่วน");
        }else if ( bmi < 25 ) {
            setTranslateResult("น้ำหนักเกิน");
        }else if ( bmi < 30 ) {
            setTranslateResult("โรคอ้วน");
        }else {
            setTranslateResult("โรคอ้วนอันตราย");
        }
    }

    return (
        <Container maxWidth='lg'>
            <Grid container spacing={2} sx={{ marginTop : "10px" }}>
                <Grid item xs={12}>
                    <Typography variant="h5"> ยินดีต้อนรับสู่เว็บคำนวณ BMI </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Box sx={{ textAlign : "left" }} >
                        คุณชื่อ: <input type="text" value={name} onChange={ (e) => { setName(e.target.value); }} /> <br /><br />
                        ส่วนสูง: <input type="text" value={heigh} onChange={ (e) => { setHeigh(e.target.value); }} /> ซม. <br /><br />
                        น้ำหนัก: <input type="text" value={weight} onChange={ (e) => { setWeight(e.target.value); }} /> กก. <br /><br />

                        <Button variant="contained"  onClick={ () => { CalculateBMI() } } >
                            Calculate
                        </Button>
                    </Box>
                </Grid>
                <Grid item xs={4}>
                    {   ( bmiResult != 0 ) && 
                            <div>
                                <hr />
                                <h1>นี่คือผลคำนวณ</h1>
                                <BMIResult 
                                    name = { name }
                                    bmi = { bmiResult }
                                    result = { translateResult }
                                />
                            </div>
                    }
                </Grid>
            </Grid>
        </Container>
    );
}

export default BMICalPage;

/* <div align="left">
            <div align="center">
                <h1>ยินดีต้อนรับสู่เว็บคำนวณ BMI</h1>
                <hr />
                <h4>
                คุณชื่อ: <input type="text" value={name} onChange={ (e) => { setName(e.target.value); }} /> <br />
                ส่วนสูง: <input type="text" value={heigh} onChange={ (e) => { setHeigh(e.target.value); }} /> ซม. <br />
                น้ำหนัก: <input type="text" value={weight} onChange={ (e) => { setWeight(e.target.value); }} /> กก. <br />
                </h4>

                <Button variant="contained"  onClick={ () => { CalculateBMI() } } >
                    Calculate
                </Button>

                {   ( bmiResult != 0 ) && 
                        <div>
                            <hr />
                            <h1>นี่คือผลคำนวณ</h1>
                            <BMIResult 
                                name = { name }
                                bmi = { bmiResult }
                                result = { translateResult }
                            />
                        </div>
                }
            </div>
        </div> */